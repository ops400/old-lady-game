#include <raylib.h>

#ifndef MOUSE_H_
#define MOUSE_H_

typedef struct mousePoint{
    Rectangle rec;
    Vector2 pos;
} mousePoint;

#endif