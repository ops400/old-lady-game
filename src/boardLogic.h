#include "button.h"
#include "mouse.h"
#include <raylib.h>
#include <sys/types.h>

#ifndef BOARD_LOGIC_H_
#define BOARD_LOGIC_H_

u_int8_t boardArrayPositionX(button btn, mousePoint mP);
u_int8_t boardArrayPositionY(button btn, mousePoint mP);
u_int8_t symbol(button btn, mousePoint mP);
u_int8_t boolFLip(button btn, mousePoint mP);
// oh boy can't wait to the fucked up if else cascade
u_int8_t whoWon(unsigned int board[3][3], bool boardFreeSpaces[3][3]);

#endif