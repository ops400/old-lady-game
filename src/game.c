#include <raylib.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>
#include "mouse.h"
#include "button.h"
#include "boardLogic.h"
#include "game.h"


u_int8_t turn;


void game(){
    // X is 1; 0 is 2
    turn = 1;
    Texture2D xSprite = LoadTexture("./src/res/X.png");
    Texture2D oSprite = LoadTexture("./src/res/O.png");

    mousePoint mousePointer;
    mousePointer.rec.height = 5;
    mousePointer.rec.width = 5;

    const Vector2 buttonPositing[3][3] = {
    {{112, 32}, {247, 32}, {391, 32}},
    {{112, 167},{247, 167},{391, 167}},
    {{112, 311},{247, 311},{391, 311}}
    };

    const Vector2 buttonSizing[3][3] = {
    {{120, 120}, {129, 120}, {120, 120}},
    {{120, 129},{129, 129},{120, 129}},
    {{120, 120},{129, 120},{120, 120}}
    };

    button buttons[3][3];
    for(u_int8_t i = 0; i < 3; i++){
        for(u_int8_t j = 0; j < 3; j++){
            buttons[i][j].color = BUTTON_NORMAL;
            buttons[i][j].rec.x = buttonPositing[i][j].x;
            buttons[i][j].rec.y = buttonPositing[i][j].y;
            buttons[i][j].rec.width = buttonSizing[i][j].x;
            buttons[i][j].rec.height = buttonSizing[i][j].y;
        }
    }
    //had no idea how to put this inside a for loop so here it is my lazy solution
    buttons[0][0].id = 0;
    buttons[0][1].id = 1;
    buttons[0][2].id = 2;
    buttons[1][0].id = 3;
    buttons[1][1].id = 4;
    buttons[1][2].id = 5;
    buttons[2][0].id = 6;
    buttons[2][1].id = 7;
    buttons[2][2].id = 8;
    // 0 is empty; 1 is X; 2 is O
    unsigned int board[3][3] = {
        {0,0,0},
        {0,0,0},
        {0,0,0}
    };

    bool boardFreeSpaces[3][3] = {
        {0,0,0},
        {0,0,0},
        {0,0,0}
    };
    // 1 is X; 2 is O
    while(!WindowShouldClose()){
        mousePointer.pos = GetMousePosition();
        mousePointer.rec.x = mousePointer.pos.x;
        mousePointer.rec.y = mousePointer.pos.y;
        switch(whoWon(board, boardFreeSpaces)){
            case 0:
                for(unsigned int i = 0; i < 3; i++){
                    for(unsigned int j = 0; j < 3; j++){
                        buttons[i][j].color = onMe(buttons[i][j], mousePointer,0);
                        if(boardFreeSpaces[boardArrayPositionX(buttons[i][j], mousePointer)][boardArrayPositionY(buttons[i][j], mousePointer)] == 0){
                            board[boardArrayPositionX(buttons[i][j], mousePointer)][boardArrayPositionY(buttons[i][j], mousePointer)] = symbol(buttons[i][j], mousePointer);
                        }
                        boardFreeSpaces[boardArrayPositionX(buttons[i][j], mousePointer)][boardArrayPositionY(buttons[i][j], mousePointer)] = boolFLip(buttons[i][j], mousePointer);
                    }
                }
                break;
            default:
                for(u_int8_t i  = 0; i < 3; i++){
                    for(u_int8_t j = 0; j < 3; j++){
                        buttons[i][j].color = BUTTON_NORMAL;
                    }
                }
        }
        BeginDrawing();
            ClearBackground(GRUVBOX_GRAY);
            for(u_int8_t i = 0; i < 3; i++){
                for(u_int8_t j = 0; j < 3; j++){
                    DrawRectangleRec(buttons[i][j].rec, buttons[i][j].color);
                }
            }
            switch(whoWon(board, boardFreeSpaces)){
                case 0:
                    symbolsDraw(board, xSprite, oSprite);
                    draw2DGrid();
                    break;
                default:
                    endOrReset(mousePointer);
                    switch(endOrReset(mousePointer)){
                        case 1:
                            CloseWindow();
                            break;
                        case 2:
                            for(u_int8_t i = 0; i < 3; i++){
                                for(u_int8_t j = 0; j < 3; j++){
                                    boardFreeSpaces[i][j] = 0;
                                    board[i][j] = 0;
                                }
                            }
                            break;
                        default:
                        ;
                    }
            }
            drawWinner(whoWon(board, boardFreeSpaces));
            DrawRectangleRec(mousePointer.rec, RED); // debug
            drawTurn(turn, xSprite, oSprite);
        EndDrawing(); 
    }
}

// nossa que solução de porco para um problema que devia ter sido facíl de resolver

u_int8_t symbol(button btn, mousePoint mP){
    u_int8_t turnBuffer = turn;
    if(buttonPressed(btn, mP)){
        printf("pressed\n");
        switch(turn){
            case 1:
                turn = 2;
                break;
            case 2:
                turn = 1;
                break;
            default:
                printf("isso nn era para acontecer\n");
        }
        return turnBuffer;
    }
    return 3;
}