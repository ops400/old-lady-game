#include <raylib.h>
#include <sys/types.h>
#include "game.h"
#include "mouse.h"
#include "button.h"

#define DEBUG_WHITE (Color){245, 245, 245, 255} // now it's a clone of RAYWHITE

#define LINE_WIDTH 15

#define V_LINE_Y_S 32
#define V_LINE_Y_E 431
#define V_LINE1_X_SE 240
#define V_LINE2_X_SE 384

#define H_LINE_X_S 112
#define H_LINE_X_E 511
#define H_LINE1_Y_SE 160
#define H_LINE2_Y_SE 304

void draw2DGrid(){
    DrawLineEx((Vector2){V_LINE1_X_SE, V_LINE_Y_S}, (Vector2){V_LINE1_X_SE, V_LINE_Y_E}, (float)LINE_WIDTH, DEBUG_WHITE);
    DrawLineEx((Vector2){V_LINE2_X_SE, V_LINE_Y_S}, (Vector2){V_LINE2_X_SE, V_LINE_Y_E}, (float)LINE_WIDTH, DEBUG_WHITE);

    DrawLineEx((Vector2){H_LINE_X_S, H_LINE1_Y_SE}, (Vector2){H_LINE_X_E, H_LINE1_Y_SE}, (float)LINE_WIDTH, DEBUG_WHITE);
    DrawLineEx((Vector2){H_LINE_X_S, H_LINE2_Y_SE}, (Vector2){H_LINE_X_E, H_LINE2_Y_SE}, (float)LINE_WIDTH, DEBUG_WHITE);
}

void symbolsDraw(unsigned int board[3][3], Texture2D symbol1, Texture2D symbol2){
    const Vector2 elementPositing[3][3] = {
    {{118, 38}, {257.5, 38}, {397, 38}},
    {{118, 177.5},{260.5, 177.5},{397, 177.5}},
    {{118, 317},{257.5, 317},{397, 317}}
    };
    //had to declare here and not pass it as an argument because it was not erasing the previous values
    for(u_int8_t i = 0; i < 3; i++){
        for(u_int8_t j = 0; j < 3; j++){
            if(board[i][j] == 1) DrawTextureEx(symbol1, elementPositing[i][j], 0.0f, 0.9f, WHITE);
            else if(board[i][j] == 2) DrawTextureEx(symbol2, elementPositing[i][j], 0.0f, 0.9f, WHITE);
            else DrawTextureEx(symbol2, elementPositing[i][j], 0.0f, 0.9f, TRANSPARENT);
        }
    }
}

void drawWinner(u_int8_t winner){
    switch(winner){
        case 1:
            DrawText("X Wins", WW/2-30, 10, 20, RAYWHITE);
            break;
        case 2:
            DrawText("O Wins", WW/2-30, 10, 20, RAYWHITE);
            break;
        case 3:
            DrawText("Tie", WW/2-30, 10, 20, RAYWHITE);
            break;
        default:
        ;
    }
}

u_int8_t endOrReset(mousePoint mP){    
    button end, reset;
    
    end.rec.width = 100;
    end.rec.height = 50;
    end.rec.y = (float)WH/2-end.rec.height/2;
    end.rec.x = (float)WW/2-end.rec.width-16;
    end.color = onMe(end, mP, 1);
    // end.color = RED;

    reset.rec.width = end.rec.width;
    reset.rec.height = end.rec.height;
    reset.rec.y = (float)WH/2-reset.rec.height/2;
    reset.rec.x = end.rec.x+reset.rec.width+20;
    // reset.color = BLUE;
    reset.color = onMe(reset, mP, 1);
    DrawRectangleRec(end.rec, end.color);
    DrawText("End", end.rec.x+30, end.rec.y+end.rec.height/3, 20, RAYWHITE);
    DrawRectangleRec(reset.rec, reset.color);
    DrawText("Restart", reset.rec.x+10, reset.rec.y+reset.rec.height/3, 20, RAYWHITE);
    switch(buttonPressed(end, mP)){
        case 1:
            return 1;
            break;
        default:
        ;    
    }
    switch(buttonPressed(reset, mP)){
        case 1:
            return 2;
            break;
        default:
        ;
    }
    return 0;
}

void drawTurn(u_int8_t turn, Texture2D symbol1, Texture2D symbol2){
    switch(turn){
        case 1:
            DrawTextureEx(symbol1, (Vector2){0, 0}, 0.0f, 0.2f, WHITE);
            DrawText("'s turn", 24, 0, 30, RAYWHITE);
            break;
        case 2:
            DrawTextureEx(symbol2, (Vector2){0, 0}, 0.0f, 0.2f, WHITE);
            DrawText("'s turn", 24, 0, 30, RAYWHITE);
            break;
        default:
            DrawText("Something went wrong", 0, 0, 20, RAYWHITE);
    }
}