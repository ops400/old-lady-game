#include <raylib.h>
#include <stdio.h>
#include <sys/types.h>
#include "button.h"
#include "mouse.h"

Color onMe(button btn, mousePoint mP, u_int8_t buttonType){
    if(buttonPressed(btn, mP)){
        return BUTTON_CLICK;
    }
    else if(CheckCollisionRecs(btn.rec, mP.rec)){
        return BUTTON_ON_TOP;
    }
    switch(buttonType){
        case 0:
            return BUTTON_NORMAL;
            break;
        case 1:
            return BUTTON_END_OR_RESET;
        default:
        ;
    }
    return BUTTON_NORMAL;
}

bool buttonPressed(button btn, mousePoint mP){
    if(CheckCollisionRecs(btn.rec, mP.rec) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) return 1;
    return 0;
}
