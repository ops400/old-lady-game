#include <raylib.h>
#include <stdio.h>
#include "button.h"
#include "game.h"
#include "mouse.h"

// something strange that I noticed, is that I switch between portuguese and english often in this project

int main(){
    SetConfigFlags(FLAG_VSYNC_HINT);
    InitWindow(WW, WH, WN);
    game();
    return 0;
}