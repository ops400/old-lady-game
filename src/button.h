#include <raylib.h>
#include <sys/types.h>
#include "mouse.h"

#ifndef BUTTON_H_
#define BUTTON_H_

#define BUTTON_NORMAL (Color){40, 40, 40, 255}
#define BUTTON_ON_TOP (Color){70, 70, 70, 255}
#define BUTTON_CLICK (Color){50, 50, 50, 255}
#define BUTTON_END_OR_RESET (Color){29, 31, 33, 255}


typedef struct button{
    Rectangle rec;
    Color color;
    int id;
} button;

// 0 board buttons, 1 endOrReset buttons
Color onMe(button btn, mousePoint mP, u_int8_t buttonType);
bool buttonPressed(button btn, mousePoint mP);

#endif