#include <raylib.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>
#include "button.h"
#include "mouse.h"

u_int8_t boardArrayPositionX(button btn, mousePoint mP){
    if(buttonPressed(btn, mP)){
        switch(btn.id){
        case 0:
            return 0;
            break;
        case 1:
            return 0;
            break;
        case 2:
            return 0;
            break;
        case 3:
            return 1;
            break;
        case 4:
            return 1;
            break;
        case 5:
            return 1;
            break;
        case 6:
            return 2;
            break;
        case 7:
            return 2;
            break;
        case 8:
            return 2;
            break;
        default:
        printf("something went wrong\n");
    }
    }
    return 3;
}

u_int8_t boardArrayPositionY(button btn, mousePoint mP){
    if(buttonPressed(btn, mP)){
        switch(btn.id){
        case 0:
            return 0;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 2;
            break;
        case 3:
            return 0;
            break;
        case 4:
            return 1;
            break;
        case 5:
            return 2;
            break;
        case 6:
            return 0;
            break;
        case 7:
            return 1;
            break;
        case 8:
            return 2;
            break;
        default:
        printf("something went wrong\n");
    }
    }
    return 3;
}

//how in the bloody hell does line 77 and 40 WORK!?
//can an array just accept an outbound value and not write it?
//or worse this extremely janky solution actually creates another array index?

// unsigned char turnFlip(button btn, mousePoint mP, unsigned char turn){
//     if(turn == 1 && buttonPressed(btn, mP)) return 2;
//     if(turn == 2 && buttonPressed(btn, mP)) return 1;
//     return turn;
// }

u_int8_t boolFLip(button btn, mousePoint mP){
    if(buttonPressed(btn, mP)){
        return 1;
    }
    return 0;
}

// 0 none; 1 X; 2 O; 3 tie
u_int8_t whoWon(unsigned int board[3][3], bool boardFreeSpaces[3][3]){
    u_int8_t numberOfFreeSpaces = 0;
    for(u_int8_t i = 0; i < 3; i++){
        for(u_int8_t j = 0; j < 3; j++){
            switch(boardFreeSpaces[i][j]){
                case 0:
                    numberOfFreeSpaces++;
                    break;
                default:
                ;
            }
        }
    }
    if(numberOfFreeSpaces > 0){
        if(
            // XXX H 0&1&2
            board[0][0] == 1 && board[0][1] == 1 && board[0][2] == 1 ||
            board[1][0] == 1 && board[1][1] == 1 && board[1][2] == 1 ||
            board[2][0] == 1 && board[2][1] == 1 && board[2][2] == 1 ||
            // XXX V 0&1&2
            board[0][0] == 1 && board[1][0] == 1 && board[2][0] == 1 ||
            board[0][1] == 1 && board[1][1] == 1 && board[2][1] == 1 ||
            board[0][2] == 1 && board[1][2] == 1 && board[2][2] == 1 ||
            // XXX D
            board[0][0] == 1 && board[1][1] == 1 && board[2][2] == 1 ||
            board[0][2] == 1 && board[1][1] == 1 && board[2][0] == 1){
                return 1;
        }
        if(
            // OOO H 0&1&2
            board[0][0] == 2 && board[0][1] == 2 && board[0][2] == 2 ||
            board[1][0] == 2 && board[1][1] == 2 && board[1][2] == 2 ||
            board[2][0] == 2 && board[2][1] == 2 && board[2][2] == 2 ||
            // OOO V 0&1&2
            board[0][0] == 2 && board[1][0] == 2 && board[2][0] == 2 ||
            board[0][1] == 2 && board[1][1] == 2 && board[2][1] == 2 ||
            board[0][2] == 2 && board[1][2] == 2 && board[2][2] == 2 ||
            // OOO D
            board[0][0] == 2 && board[1][1] == 2 && board[2][2] == 2 ||
            board[0][2] == 2 && board[1][1] == 2 && board[2][0] == 2){
                return 2;
        }
    }
    else return 3;
    return 0;
    // is this too bad?
    // que coisa horrorosa! será que tem um jeito melhor de fazer isso?
}