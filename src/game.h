#ifndef GAME_H_
#define GAME_H_

#include <sys/types.h>
#include <raylib.h>
#include "mouse.h"
#define GRUVBOX_GRAY (Color){40, 40, 40, 255}
#define TRANSPARENT (Color){0, 0, 0, 0}
#define WW 640
#define WH 480
#define WN "old lady game"

void game();
void draw2DGrid();
void symbolsDraw(unsigned int board[3][3], Texture2D symbol1, Texture2D symbol2);
void drawWinner(u_int8_t winner);
u_int8_t endOrReset(mousePoint mP);
void drawTurn(u_int8_t turn, Texture2D symbol1, Texture2D symbol2);

#endif